<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('configuration.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}


	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}

//$user_name = $_GET['user_name'];
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	
	//Sanitize the POST values
	$yourself = clean($_POST['yourself']);
	$books_read = clean($_POST['books_read']);
	$writing_genre = clean($_POST['writing_genre']);
	$favorite_book = clean($_POST['favorite_book']);
	$favorite_author = clean($_POST['favorite_author']);
if (isset($_POST['adventure'])){
			$adventure = 1;
		}
		else{
			$adventure = 0;
		}

if (isset($_POST['romance'])){
			$romance = 1;
		}
		else{
			$romance = 0;
		}

if (isset($_POST['suspense'])){
			$suspense = 1;
		}
		else{
			$suspense = 0;
		}

if (isset($_POST['mystery'])){
			$mystery = 1;
		}
		else{
			$mystery = 0;
		}

if (isset($_POST['fantasy'])){
			$fantasy = 1;
		}
		else{
			$fantasy = 0;
		}

if (isset($_POST['non_fiction'])){
			$non_fiction = 1;
		}
		else{
			$non_fiction = 0;
		}

//Input Validations
	if($yourself == '') {
		$errmsg_arr[] = 'Describe yourself is missing';
		$errflag = true;
	}
	if($books_read == '') {
		$errmsg_arr[] = 'Types of books read is missing';
		$errflag = true;
	}
	if($writing_genre == '') {
		$errmsg_arr[] = 'Writing genres is missing';
		$errflag = true;
	}
	if($favorite_book == '') {
		$errmsg_arr[] = 'Favorite book is missing';
		$errflag = true;
	}
	if($favorite_author == '') {
		$errmsg_arr[] = 'Favorite author is missing';
		$errflag = true;
	}

//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: register-success.php");
		exit();
	}


//Create INSERT query
	//$qry = "INSERT INTO profile(`Yourself`, `BooksRead`, `WritingGenre`, FavoriteBook, FavoriteAuthor) VALUES('$yourself', '$books_read', '$writing_genre,' //'$favorite_book', '$favorite_author')";

$qry= "INSERT INTO `profile`( `Yourself`, `BooksRead`, `WritingGenre`, `FavoriteBook`, `FavoriteAuthor`, `Adventure`, `Romance`, `Suspense`, `Mystery`, `Fantasy`, `NonFiction`) VALUES ('$yourself', '$books_read', '$writing_genre', '$favorite_book', '$favorite_author', '$adventure', '$romance', '$suspense', '$mystery', '$fantasy', '$non_fiction')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {
		header("location: story.php");
		exit();
	}else {
		die("Query failed");
	}

	
?>