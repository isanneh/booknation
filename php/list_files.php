<?php
//Start session
session_start();
// Connect to the database
$dbLink = new mysqli('localhost', 'root', 'abc123', 'test');
if(mysqli_connect_errno()) {
    die("MySQL connection failed: ". mysqli_connect_error());
}
 ?>
 <html>
 <body>
 <title> File List </title>
 <link href="loginmodule.css" rel="stylesheet" type="text/css" />
 <a href="home-form.php">Home</a><br />
 <?php
// Query for a list of all existing files
if(isset($_SESSION['SESS_PRIV']) && ((trim($_SESSION['SESS_PRIV']) == 'Admin')||(trim($_SESSION['SESS_PRIV']) == 'Privileged'))){
	
	$sql = 'SELECT * FROM `file` where `status`=1'; 
}
else{
	$sql = 'SELECT * FROM `file` where `status`=1 and `private` = 0';
}

$result = $dbLink->query($sql);
 

// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {
        // Print the top of a table
        echo '<table width="100%">
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Mime</b></td>
                    <td><b>Size (bytes)</b></td>
                    <td><b>Created</b></td>
					<td><b>Uploader</b></td>
                    <td><b>&nbsp;</b></td>
                </tr>';
 
        // Print each file
        while($row = $result->fetch_assoc()) {
            echo "
                <tr>
                    <td><a href=\"filestat.php?file=".$row['id']."\">{$row['name']}</td>
                    <td>{$row['mime']}</td>
                    <td>{$row['size']}</td>
                    <td>{$row['created']}</td>
					<td>{$row['uploader']}</td>
                    <td><a href='get_file.php?id={$row['id']}'>Download</a></td>
                </tr>";
        }
 
        // Close table
        echo '</table>';
    }
 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>