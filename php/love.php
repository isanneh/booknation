<?php
	//Start session
	session_start();
	//require_once('SAauth.php');
	
	//Include database connection details
	require_once('configuration.php');
	// Connect to the database
	
	$dbLink = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if(mysqli_connect_errno()) {
		die("MySQL connection failed: ". mysqli_connect_error());
	}


?>

<!doctype html>
<html>
<title>Home</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
<body>

<?php
if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'member')){
	echo "
<table width=\"100%\">
<tr>

	
	
<td align=\"center\" color: #99CC00;
	margin: 0px 0px 5px;
	padding: 0px 0px 3px;
	font: bold 18px Verdana, Arial, Helvetica, sans-serif;>	
<a href=\"files.php\">Submit A Message </a>
<a href=\"adventure.php\">Update A Message</a>
<a href=\"romance.php\">Find A Message</a>

	</td>
</tr>
</table>
<br />
";
}	


else{

	echo "
<table width=\"100%\">
<tr>
	
<td align=\"left\" color: #99CC00;
	font: bold 18px Verdana, Arial, Helvetica, sans-serif;>	
<a href=\"files.php\">All Stories</a>
<a href=\"adventure.php\">Adventure</a>
<a href=\"romance.php\">Romance</a>
<a href=\"suspense.php\">Suspense</a>
<a href=\"mystery.php\">Mystery</a>
<a href=\"fantasy.php\">Fantasy</a>
<a href=\"nonfiction.php\">Non Fiction</a>
<a href=\"story.php\">Submit Story</a>
	</td>
<td align=\"right\" color: #99CC00;
	
	font: bold 18px Verdana, Arial, Helvetica, sans-serif;>	
<a href=\"login-form.php\">Log in</a>
<a href=\"register-form.php\">Register</a>
	</td>
</tr>
</table>
<br />
";
}

?>

<h1 style="text-align:center"> Stories: </h1>
<?php
// Query for a list of all existing files
$sql = 'SELECT * FROM `story';
$result = $dbLink->query($sql);
$i = 0;
// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {
        // Print the top of a table
        echo '<form action="filetablefunctions.php" method="post">
			<table width="100%" border="1" cellpadding="0" >
                <tr>
                <td class=tabhead><br /><b>User Name</b></td>
		<td class=tabhead><br /><b>Title</b></td>
		<td class=tabhead><br /><b>Description</b></td>
		<td class=tabhead><br /><b>Views</b></td>
		<td class=tabhead><br /><b>Created</b></td>
	
		
                
            </tr>';
 
        // Print each file
        while($row = $result->fetch_assoc()) {
            echo "<tr valign='middle'>";
//$slink=$row['Title'];
//$slink .=".php";
//$member = mysql_fetch_assoc($result);
			//$slink = $member['Title'];
//$link= $row['Title'];
//$slink .=".php";
//extract($row['UserName'].");

//$a= array([0] => ".$row['UserName'].");
//$_SESSION['SESS_test']=".$row['UserName'].";


//ob_start();
		//echo "<noscript>".$row['UserName']."</noscript>";
//echo 'yes';
//$myStr = ob_get_contents();
//ob_end_clean();
//$myStr.= ".php";
//ob_start();
//echo 'Hello World';
//$myStr = ob_get_contents();
//ob_end_clean();
//$at= '123';
echo '<td width="10%">'.$row['UserName'].'</td>'; 
		//echo "<td>".$row['UserName']."</td>";
echo '<td width="20%"><a href=\'' . $row['id'] . '.php \'>'.$row['Title'].'</a></td>';
		//echo '<td>'.$row['Title'].'</td>';
		echo '<td width="50%">'.$row['Description'].'</td>';
		echo '<td width="5%">'.$row['Views'].'</td>';
		echo '<td width="15%">'.$row['Created'].'</td>';
		//echo '<td><input name="username['.$i++.']" value='.$row['Written By:'].' type="submit"></td>';
		//echo '<td><input name="title['.$i++.']" value='.$row['Title'].' type="submit"></td>';
		//echo '<td><input name="description['.$i++.']" value='.$row['Description'].' type="submit"></td>';
		//echo '<td><input name="views['.$i++.']" value='.$row['Views'].' type="submit"></td>';
		//echo '<td><input name="created['.$i++.']" value='.$row['Created'].' type="submit"></td>';
		//echo '<td><input name="delete['.$i++.']" value='.$row['member_id'].' type="checkbox"></td>';
		//echo '<td><input name="read['.$i++.']" value='.$row['id'].' type="submit"></td>';
		//echo "<td>" "<a href="test">" Click to read"</a>" "</td>";
//echo "<td><a href=\"" . $row['Title'] . ".php \">NAME OF YOUR LINK</a></td>";
//echo "<a href="http://www.w3schools.com">Visit W3Schools</a>";

	
		echo "</tr>";
        }
 
        // Close table
        echo '</table>
		
		</form>';
    }
 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>
</body>
</html>