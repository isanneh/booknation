<?php
	session_start();

include('menu.php');
//Include database connection details
	require_once('configuration.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}


	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}


if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'member')){
}
else
{
header("location: accessDenied.php");
			exit();
}

?>
<!doctype html>
<html>
<title>User Control</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
<body>
<?php



	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}


?>
<div align="center">
You must enter your password and check the password and/or email address field(s) in order to make changes
</div>
<br />
<form id="loginForm" name="loginForm" method="post" action="uc-edit.php">
  <table width="300" border="0" align="center" cellpadding="2" cellspacing="0">
  <tr>
      <th>Current Password</th>
      <td alight="center"><input name="oldpassword" type="password" class="textfield" id="oldpassword" /></td>
    </tr>
	<tr>
      <th>Email Address</th>
      <td><input name="eedit" type="checkbox"><input name="email" type="text" class="textfield" id="email" value="<?php echo $_SESSION['SESS_EMAILADDRESS']  ?>"  /></td>
    </tr>
    <tr>
      <th>New Password</th>
      <td><input name="pedit" type="checkbox"><input name="newpassword" type="password" class="textfield" id="newpassword"  /></td>
    </tr>
    <tr>
      <th>Confirm New Password </th>
      <td><input name="cpassword" type="password" class="textfield" id="cpassword"  /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Edit Details" /></td>
    </tr>
  </table>
</form>
</body>
</html>
