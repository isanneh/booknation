<?php
	session_start();

//Include database connection details
	require_once('configuration.php');
	
	//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}
	
	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Registration Successful</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>
<h1 style="text-align:center">Next Step  </h1>
<h2 align="center">Fill out the following information to complete your registration! </h2>




<form id="loginForm" name="loginForm" method="post" action="nextregister-exec.php">

  <p>Describe yourself:</p>


<textarea name="yourself" id="styled" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')">Enter your comment here...</textarea>
     
      <p>What types of books do you read?</p>


<textarea name="books_read" id="styled" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')">Enter your comment here...</textarea>
      
 <p>If you are a writer, what genre of writings do you write?</p>


<textarea name="writing_genre" id="styled" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')">Enter your comment here...</textarea>

 <p>What's your favorite book?</p>


<textarea name="favorite_book" id="styled" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')">Enter your comment here...</textarea>

 <p>Who's your favorite author?</p>


<textarea name="favorite_author" id="styled" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')">Enter your comment here...</textarea>

<table>
<tr>
<td><b>Types of books you're interested in:</b></td>
   
<td ><input name="adventure" value='SET' type="checkbox"> Adventure</td>
<td ><input name="romance" value='SET' type="checkbox"> Romance</td>
<td ><input name="suspense" value='SET' type="checkbox"> Suspense</td>
<td ><input name="mystery" value='SET' type="checkbox"> Mystery</td>
<td ><input name="fantasy" value='SET' type="checkbox"> Fantasy</td>
<td ><input name="non_fiction" value='SET' type="checkbox"> Non Fiction</td>
    </tr>
  
  
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="submit" /></td>
    </tr>
  </table>

</body>
</html>
