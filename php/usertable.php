<?php

	require_once('SAauth.php');
	
	//Include database connection details
	require_once('config.php');
	
	//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}
	
	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}
?>
<html>
<title>User Table</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
    <body>
	<a href="system.php">Back</a>
        <h2>User Table</h2>
		<form action="usertablefunctions.php" method="post">
			<table width ="100%" border="2" cellpadding="4" cellspacing="4">
            <tr>
                <td class=tabhead><br /><b>First Name</b></td>
                <td class=tabhead><br /><b>Last Name</b></td>
				<td class=tabhead><br /><b>Email</b></td>
				<td class=tabhead><br /><b>Username</b></td>
                <td class=tabhead><br /><b>Credits</b></td>
				<td class=tabhead><br /><b>Status</b></td>
                <td class=tabhead><br /><b>Privileges</b></td>
				<td class=tabhead><br /><b>Approve</b></td>
				<td class=tabhead><br /><b>Delete</b></td>
				<td class=tabhead><br /><b>Warn</b></td>
            </tr>
<?php
	$result = mysql_query("SELECT * FROM users");
	$i = 0;
	while ($row = mysql_fetch_array($result)) {
		echo "<tr valign='middle'>";
		echo "<td>".$row['firstname']."</td>";
		echo "<td>".$row['lastname']."</td>";
		echo "<td>".$row['email']."</td>";
		echo "<td>".$row['login']."</td>";
		echo "<td>".$row['credit']."</td>";
		echo "<td>".$row['status']."</td>";
		echo "<td>".$row['privilege']."</td>";
		echo '<td><input name="approve['.$i++.']" value='.$row['member_id'].' type="checkbox"></td>';
		echo '<td><input name="delete['.$i++.']" value='.$row['member_id'].' type="checkbox"></td>';
		echo '<td><input name="warn['.$i++.']" value='.$row['member_id'].' type="checkbox"></td>';
		echo "</tr>";
	}
?>
		</table>
		<br />
		<input type="submit" name='Submit' value="Confirm Approved">
		<input type="submit" name='Submit' value="Confirm Delete">
		<input type="submit" name='Submit' value="Confirm Warn">
		</form>

    </body>
</html>
