<html>
<link href="/menu_styles.css" rel="stylesheet" type="text/css">
<?php
if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'member')){
	echo "


<div id=\"cssmenu\">
<ul>
  

<li class=\"active\"><a href=\"index.php\"><span>All Stories</span></a></li>
<li><a href=\"adventure.php\"><span>Adventure</span></a></li>
<li><a href=\"romance.php\"><span>Romance</span></a></li>
<li><a href=\"suspense.php\"><span>Suspense</span></a></li>
<li><a href=\"mystery.php\"><span>Mystery</span></a></li>
<li><a href=\"fantasy.php\"><span>Fantasy</span></a></li>
<li><a href=\"nonfiction.php\"><span>Non Fiction</span></a></li>
<li><a href=\"story.php\"><span>Submit Story</span></a></li>
</ul>
</div>	
<br />
<div id=\"button\">
<ul>
	<li><a href=\"mystories.php\"><span>My Stories</span></a></li>
	<li><a href=\"delete.php\">Delete Story</a></li>	
	<li><a href=\"user-controls.php\">Account Settings</a></li>
	<li><a href=\"logout.php\">Logout</a></li>
</ul>
</div>	
<br />
";
}	


else{

	echo "

<div id=\"cssmenu\">
<ul>
  

<li class=\"active\"><a href=\"files.php\"><span>All Stories</span></a></li>
<li><a href=\"adventure.php\"><span>Adventure</span></a></li>
<li><a href=\"romance.php\"><span>Romance</span></a></li>
<li><a href=\"suspense.php\"><span>Suspense</span></a></li>
<li><a href=\"mystery.php\"><span>Mystery</span></a></li>
<li><a href=\"fantasy.php\"><span>Fantasy</span></a></li>
<li><a href=\"nonfiction.php\"><span>Non Fiction</span></a></li>
<li><a href=\"story.php\"><span>Submit Story</span></a></li>
</ul>
</div>	

<br />
";
}

?>
</html>
