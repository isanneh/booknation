<?php
	session_start();
include('menu.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Member Registration</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>
<p>&nbsp;</p>
<h1 style="text-align:center"> Member Registration </h1>
<h2  > Please enter your information below:</h2>
<form id="loginForm" name="loginForm" method="post" action="register-exec.php">
  <table width="500" border="0" align="left" cellpadding="2" cellspacing="0">
    <tr>
      <th>First Name </th>
      <td><input name="first_name" type="text" class="textfield" id="first_name" /></td>
    </tr>
    <tr>
      <th>Last Name </th>
      <td><input name="last_name" type="text" class="textfield" id="last_name" /></td>
    </tr>
    <tr>
      <th width="124">Choose a username</th>
      <td width="168"><input name="user_name" type="text" class="textfield" id="user_name" /></td>
    </tr>
	<tr>
      <th>Email Address </th>
      <td><input name="email_address" type="text" class="textfield" id="email_address" /></td>
    </tr>



    <tr>
      <th>Choose a password</th>
      <td><input name="password" type="password" class="textfield" id="password" /></td>
    </tr>
    <tr>
      <th>Confirm Password </th>
      <td><input name="confirm_password" type="password" class="textfield" id="confirm_password" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name='submit' id='submit' value="Register" /></td>
    </tr>
  </table>
</form>
</body>
</html>
