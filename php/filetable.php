<?php

	//require_once('SAauth.php');
	
	//Include database connection details
	require_once('configuration.php');
	// Connect to the database
	
	$dbLink = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if(mysqli_connect_errno()) {
		die("MySQL connection failed: ". mysqli_connect_error());
	}

?>

<html>
<title>File Table</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
<body>
<a href="system.php">Back</a>
<h2>User Table</h2>
<?php
// Query for a list of all existing files
$sql = 'SELECT * FROM `file`';
$result = $dbLink->query($sql);
$i = 0;
// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {
        // Print the top of a table
        echo '<form action="filetablefunctions.php" method="post">
			<table width="100%">
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Mime</b></td>
                    <td><b>Size (bytes)</b></td>
                    <td><b>Created</b></td>
					<td><b>Uploader</b></td>
					<td><b>Status</b></td>
					<td><b>Private</b></td>
					<td><b>Approve</b></td>
					<td><b>Delete</b></td>
                    <td><b>&nbsp;</b></td>
                </tr>';
 
        // Print each file
        while($row = $result->fetch_assoc()) {
            echo "
                <tr>
                    <td>".$row['name']."</td>
                    <td>".$row['mime']."</td>
                    <td>".$row['size']."</td>
                    <td>".$row['created']."</td>
					<td>".$row['uploader']."</td>
					<td>".$row['status']."</td>
					<td>".$row['private']."</td>
					<td><input name=\"approve['".$i++."']\" value='".$row['id']."' type=\"checkbox\"></td>
					<td><input name=\"delete['".$i++."]\" value='".$row['id']."' type=\"checkbox\"></td>
                 
                </tr>";
        }
 
        // Close table
        echo '</table>
		<br />
		<input type="submit" name=\'Submit\' value="Confirm Approved">
		<input type="submit" name=\'Submit\' value="Confirm Delete">
		</form>';
    }
 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>

</body>
</html>