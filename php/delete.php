
<?php
session_start();
	//require_once('SAauth.php');
include('menu.php');

	//require_once('SAauth.php');
	
	//Include database connection details
	require_once('configuration.php');
	// Connect to the database
	
	$dbLink = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if(mysqli_connect_errno()) {
		die("MySQL connection failed: ". mysqli_connect_error());
	}

if(isset($_SESSION['SESS_PRIV']) && (trim($_SESSION['SESS_PRIV']) == 'member')){
}
else
{
header("location: accessDenied2.php");
			exit();
}

?>

<html>
<title>Delete Stories</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
<body>
<h1 style="text-align:center"> Delete Story </h1>
<?php
$user=$_SESSION['SESS_USERNAME'];
// Query for a list of all existing files
$sql = "SELECT * FROM story WHERE `UserName`= '$user'";
$result = $dbLink->query($sql);
$i = 0;
// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>You have not added any stories!</p>';
    }
    else {
        // Print the top of a table
        
 echo '<form action="delete-exec.php" method="post">
			<table width ="100%" border-bottom="double" cellpadding="0" cellspacing="1">


            <tr>
		<td class=tabhead><br /><b>Title</b></td>
		<td class=tabhead><br /><b>Description</b></td>
		<td class=tabhead><br /><b>Views</b></td>
		<td class=tabhead><br /><b>Created</b></td>
		<td class=tabhead><br /><b>Check </b></td>
		 <td><b>&nbsp;</b></td> 
            </tr>';
                
 
        // Print each file
        while($row = $result->fetch_assoc()) {
echo "<tr valign='middle'>";
		echo '<td width="20%"><a href=\'' . $row['Title'] . '.php \'>'.$row['Title'].'</a></td>';
		echo '<td width="50%">'.$row['Description'].'</td>';
		echo '<td width="5%">'.$row['Views'].'</td>';
		echo '<td width="15%">'.$row['Created'].'</td>';
		echo '<td width="10%"><input name="delete[]" value='.$row['id'].' type="checkbox"></td>';
		echo "</tr>";
 }
 
        // Close table
        echo '</table>
		<br />
		<input type="submit" name="submit" id="submit" value="Delete">
		</form>';
    }

 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>

</body>
</html>
