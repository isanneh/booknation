<?php
	//Start session
	session_start();

include('menu.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Access Denied</title>
<link href="loginmodule.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h1 style="text-align:center"> Access Denied! </h1>
<p align="center">&nbsp;</p>
<h4 align="center" class="err">You do not have access to this page!</h4>
<p align="center">Click <a href="login-form.php">here to login</a> or click <a href="register-form.php">here to become a member now</a></p>
</body>
</html>
