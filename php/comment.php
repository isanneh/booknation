<?php
	//Start session
	session_start();
	//require_once('SAauth.php');
	
	//Include database connection details
	require_once('configuration.php');
	// Connect to the database
	
	$dbLink = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if(mysqli_connect_errno()) {
		die("MySQL connection failed: ". mysqli_connect_error());
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Comments  with jQuery and Ajax</title>

  
 <script type="text/javascript" src="jquery.js"></script>
 <script type="text/javascript">
$(function() {

$(".submit").click(function() {

var name = $("#name").val();
var email = $("#email").val();
	var comment = $("#comment").val();
    var dataString = 'name='+ name + '&email=' + email + '&comment=' + comment;
	
	if(name=='' || email=='' || comment=='')
     {
    alert('Please Give Valide Details');
     }
	else
	{
	$("#flash").show();
	$("#flash").fadeIn(400).html('<img src="ajax-loader.gif" align="absmiddle">&nbsp;<span class="loading">Loading Comment...</span>');
$.ajax({
		type: "POST",
  url: "commentajax.php",
   data: dataString,
  cache: false,
  success: function(html){
 
  $("ol#update").append(html);
  $("ol#update li:last").fadeIn("slow");
  document.getElementById('email').value='';
   document.getElementById('name').value='';
    document.getElementById('comment').value='';
	$("#name").focus();
 
  $("#flash").hide();
	
  }
 });
}
return false;
	});



});


</script>
<style type="text/css">
body
{
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
}

#cover
{
	float: left;
	padding: 20px 0;
	margin: 2% 0 0 2%;
	border:1px solid blue;
	border-radius: 15px;
	width= "100%";
	background: #fff;
	
}

.comment_box
{
background-color:#D3E7F5; border-bottom:#ffffff solid 1px; padding-top:3px
}
a
	{
	text-decoration:none;
	color:#d02b55;
	}
	a:hover
	{
	text-decoration:underline;
	color:#d02b55;
	}
	*{margin:0;padding:0;}
	
	
	ol.timeline
	{list-style:none;font-size:1.2em;}
	ol.timeline li{ display:none;position:relative;padding:.7em 0 .6em 0;}ol.timeline li:first-child{}
	
	#main
	{
	width:500px; margin-top:20px; margin-left:100px;
	font-family:"Trebuchet MS";
	}
	#flash
	{
	margin-left:100px;
	
	}
	.box
	{
	height:85px;
	border-bottom:#dedede dashed 1px;
	margin-bottom:20px;
	}
		input
	{
	color:#000000;
	font-size:14px;
	border:#666666 solid 2px;
	height:24px;
	margin-bottom:10px;
	width:200px;
	
	
	}
	textarea
	{
	color:#000000;
	font-size:14px;
	border:#666666 solid 2px;
	height:124px;
	margin-bottom:10px;
		width:200px;
	
	}
	.titles{
	font-size:13px;
	padding-left:10px;
	
	
	}
	.star
	{
	color:#FF0000; font-size:16px; font-weight:bold;
	padding-left:5px;
	}
</style>
</head>

<body>
<div style="background:url(http://lh4.ggpht.com/_N9kpbq3FL74/SdxnYQti7XI/AAAAAAAABcQ/JmDyIsUOons/feedbirdl.jpg) right no-repeat #FFFFFF; height:90px; border-bottom:#dedede dashed 2px; padding-left:10px; ">




  <h2>More tutorials <a href="http://9lessons.blogspot.com" style="color:#0099CC">9lessons.blogspot.com</a></h2><br />
  <span style="float:right; padding-right:70px"><h3><a href="http://feeds2.feedburner.com/9lesson">Subscribe to my feeds</a></h3></span> <span style="float:left; "><h3><a href="http://9lessons.blogspot.com/2009/06/comment-system-with-jquery-ajax-and-php.html">Tutorial Link</a>&nbsp;&nbsp;&nbsp;Follow me on <a href="http://twitter.com/9lessons" target="_blank">Twitter</a></h3></span>     </div>





<?php
// Query for a list of all existing files
$sql = 'SELECT * FROM `comments` ';
$result = $dbLink->query($sql);
$i = 0;
// Check if it was successfull
if($result) {
    // Make sure there are some files in there
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {




     

 
        // Print each file
        while($row = $result->fetch_assoc()) {





          //  echo "<tr valign='middle'>";


//echo '<div id="cover">' .$row['name']. '</div>';

	//echo	'<div class="box">' .$row['name']. '</div>';
	//echo	'<p>'  .$row['comment']. '</p>';

//echo '</div>';

//echo '<h1 style="text-align:center"> "  '.$row['Title']. ' "   written by '.$row['FirstName'].' '.$row['LastName'].'  </h1>';


echo '<li class="box">
<img src="http://www.gravatar.com/avatar.php?gravatar_id=<?php echo $image; ?>" style="float:left; width:80px; height:80px; margin-right:20px"/><span style="font-size:16px; color:#663399; font-weight:bold"> '.$row['name'].' </span> <br /><br />

'.$row['LastName'].'
</li>';
		

	
		
        }
 
        
    }
 
    // Free the result
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$dbLink->error}</pre>";
}
 
// Close the mysql connection
$dbLink->close();
?>




<div id="main">
<div style="font-family:'Georgia', Times New Roman, Times, serif; font-size:2.2em; margin-bottom:30px ">
9lessons | Programming blog
</div>
<ol  id="update" class="timeline">
</ol>
<div id="flash" align="left"  ></div>

<div style="margin-left:100px">
<form action="#" method="post">
<input type="text" name="title" id="name"/><span class="titles">Name</span><span class="star">*</span><br />

<input type="text" name="email" id="email"/><span class="titles">Email</span><span class="star">*</span><br />

<textarea name="comment" id="comment"></textarea><br />

<input type="submit" class="submit" value=" Submit Comment " />
</form>
</div>






</div>
</body>
</html>
